let webpack = require('webpack');
let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');

let SRC_DIR = path.resolve(__dirname, "src");
let DIST_DIR = path.resolve(__dirname, "dist");

let config = {
  entry: SRC_DIR + '/index.js',
  output: {
    path: DIST_DIR + "/",
    filename: "bundle.js",
    publicPath: "/"
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: DIST_DIR,
    inline: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: SRC_DIR + '/index.html'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js?/,
        include: SRC_DIR,
        loader: "babel-loader",
        query: {
          presets: ["react", "es2015", "stage-2"]
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  }
}

module.exports = config